const {path} = require('@vuepress/shared-utils')

module.exports = (options) => ({
    alias: {
        '@SearchBox':
            path.resolve(__dirname, 'MLDeepSearchCyberKI.vue')
    },

    define: {
        SEARCH_MAX_SUGGESTIONS: options.searchMaxSuggestions || 5,
        SEARCH_PATHS: options.test || null,
        SEARCH_HOTKEYS: options.searchHotkeys || ['s', '/'],
        PATH_TO_DOCUMENTS: options.pathToDocuments, //|| //toDo default value,
        INDEX: options.index, //|| //toDo default value
        REF: options.ref,
        FIELDS: options.fields
    }
})
